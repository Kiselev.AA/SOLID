﻿namespace SOLID.Interfaces
{
    interface IOutput
    {
        public void Write(List<string> log);
        public void Write(string message);
    }
}
