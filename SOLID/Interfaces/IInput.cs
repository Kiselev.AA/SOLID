﻿namespace SOLID.Interfaces
{
    interface IInput
    {
        public int IntRead(string name);
    }
}
