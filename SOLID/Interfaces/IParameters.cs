﻿namespace SOLID.Interfaces
{
    interface IParameters
    {
        public int StartNumb { get; }
        public int EndNumb { get; }
        public int TriesNumb { get; }
        public IInput Input { get; }
        public IOutput Output { get; }
    }
}
