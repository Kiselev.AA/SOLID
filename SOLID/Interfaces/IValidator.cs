﻿namespace SOLID.Interfaces
{
    interface IValidator //D
    {
        public bool OutputLog { get; set; }
        public bool CheckParameters(IParameters parameters);
    }
}
