﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.IO
{
    using Interfaces;
    class MyFileOutput : IOutput
    {
        private string _path;
        public string Path { get { return _path; } }
        public MyFileOutput(string path)
        {
            _path = path;
        }

        public void Write(List<string> log)
        {
            foreach(string logItem in log)
            {

                File.AppendAllText(_path, logItem + '\n');
            }
        }
        public void Write(string message)
        {

            File.AppendAllText(_path, message + '\n');
        }
    }
}
