﻿namespace SOLID.IO
{
    using Interfaces;
    class MyConsoleOutput : IOutput  //S
    {
        public void Write(List<string> log)
        {
            foreach (var logItem in log)
            {
                Console.WriteLine(logItem);
            }
        }
        public void Write(string message)
        {
            Console.WriteLine(message);
        }
    }
}
