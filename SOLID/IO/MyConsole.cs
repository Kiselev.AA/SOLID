﻿namespace SOLID.IO
{
    using Interfaces;
    class MyConsole : IIO
    {
        public int IntRead(string name)
        {
            int result = 0;
            Console.Write("{0}: ", name);
            while (!int.TryParse(Console.ReadLine(), out result))
            {
                Console.WriteLine("{0} changed BAD, please retry...", name);
                Console.Write("{0}: ", name);
            }
            Console.WriteLine("{0} changed GOOD!", name);
            return result;
        }
        public void Write(List<string> log)
        {
            foreach (var logItem in log)
            {
                Console.WriteLine(logItem);
            }
        }
        public void Write(string message)
        {
            Console.WriteLine(message);
        }
    }
}
