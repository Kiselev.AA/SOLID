﻿namespace SOLID
{
    using Interfaces;
    class Game
    {
        private IParameters _parameters;
        public Game(IParameters parameters)
        {
            this._parameters = parameters;
        }
        public void Execute()
        {
            _parameters.Output.Write("Game is started!");

            Random random = new Random();
            int answer = random.Next(_parameters.StartNumb, _parameters.EndNumb);

            for (int i = 1; i <= _parameters.TriesNumb; i++)
            {
                if (Round(_parameters.Input.IntRead("Try " + i + " from " + _parameters.TriesNumb + ": "), answer))
                {
                    _parameters.Output.Write("Congratulation! You guessed number!");
                    break;
                }
                if (_parameters.TriesNumb == i)
                {
                    _parameters.Output.Write("Sorry, your tries lose and you don't guessed number...");
                }
            }
            _parameters.Output.Write("End Game!");
        }
        private bool Round(int variant, int answer)
        {
            bool result = false;

            if (variant == answer)
            {
                result = true;
            }
            else
            {
                if (variant > answer)
                {
                    _parameters.Output.Write("Your variant more...");
                }
                else
                {
                    _parameters.Output.Write("Your variant less...");
                }
            }
            return result;
        }
    }
}
