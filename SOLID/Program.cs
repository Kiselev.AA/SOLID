﻿namespace SOLID
{
    using Interfaces;
    using IO;
    using Params;
    class Program
    {
        static void Main(string[] args)
        {
            MyConsole myInput = new MyConsole();
            //MyConsole myOutput = new MyConsole();
            MyFileOutput myOutput = new MyFileOutput($"D:\\1.txt");
            IValidator validator = new Validator(false);
            IParameters parameters;
            do
            {
                parameters = new ParametersUser(myInput, myOutput);
            } while (!validator.CheckParameters(parameters));
                
            
            Game game = new Game(parameters);
            game.Execute();

        }
    }
}