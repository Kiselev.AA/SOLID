﻿namespace SOLID.Params
{
    using Interfaces;

    internal class ParametersDefoult : Parameters
    {
        public ParametersDefoult(IInput input, IOutput output) : base(input, output)
        {
            output.Write("Hello, gamer! Changed defoult parameters (Range - 1 to 10, Tries - 5)...");
            StartNumb = 1;
            EndNumb = 10;
            TriesNumb = 5;
        }
    }
}
