﻿namespace SOLID.Params
{
    using Interfaces;

    abstract class Parameters : IParameters
    {
        public int StartNumb { get; protected set; }
        public int EndNumb { get; protected set;  }
        public int TriesNumb { get; protected set;  }
        public IInput Input { get; protected set; }
        public IOutput Output { get; protected set;  }

        public Parameters(IInput input, IOutput output)
        {
            Input = input;
            Output = output;
        }
    }
}
