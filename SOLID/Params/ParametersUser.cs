﻿using SOLID.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Params
{
    class ParametersUser : Parameters
    {
        public ParametersUser(IInput input, IOutput output) : base(input, output)
        {
            output.Write("Hello, gamer! Please change range and number of tries...");
            StartNumb = input.IntRead("Start number");
            EndNumb = input.IntRead("End number");
            TriesNumb = input.IntRead("Tries Number");
        }
    }
}
