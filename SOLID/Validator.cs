﻿namespace SOLID
{
    using Interfaces;
    class Validator : IValidator
    {
        private List<string> outputs = new List<string> { };
        private bool outputVisible = false;
        public bool OutputLog { get { return outputVisible; } set { outputVisible = value; } }
        public Validator() { }
        public Validator(bool outputVisible)
        {
            this.outputVisible = outputVisible;
        }

        public bool CheckParameters(IParameters parameters)
        {
            outputs.Add("Verification words...");
            bool result = CheckRange(parameters.StartNumb, parameters.EndNumb) && CheckTries(parameters.TriesNumb);
            if (OutputLog)
            {
                parameters.Output.Write(outputs);
            }
            outputs.Clear();
            return result;
        }
        private bool CheckRange(int start, int finish)
        {
            bool result = start <= finish;
            if (result)
            {
                outputs.Add("Start numb - GOOD");
                outputs.Add("End numb - GOOD");
            }
            else
            {
                outputs.Add("Start numb - BAD");
                outputs.Add("End numb - BAD");
            }
            return (result);
        }
        private bool CheckTries(int tries)
        {
            bool result = tries > 0;
            if (result)
            {
                outputs.Add("End numb - GOOD");
            }
            else
            {
                outputs.Add("End numb - BAD");
            }
            return result;
        }

    }
}
